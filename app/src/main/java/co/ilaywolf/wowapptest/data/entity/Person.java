package co.ilaywolf.wowapptest.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

public  class Person implements Parcelable {
    /**
     * firstName : ROY
     * lastName : WILLIAMSON
     * statusIcon : online
     * statusMessage :
     */

    private String firstName;
    private String lastName;
    private String statusIcon;
    private String statusMessage;

    protected Person(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        statusIcon = in.readString();
        statusMessage = in.readString();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatusIcon() {
        return statusIcon;
    }

    public void setStatusIcon(String statusIcon) {
        this.statusIcon = statusIcon;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(statusIcon);
        dest.writeString(statusMessage);
    }
}
