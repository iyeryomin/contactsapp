package co.ilaywolf.wowapptest.data.entity;

import android.os.Parcel;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

public class Group extends ExpandableGroup<Person> {
    /**
     * groupName : friends
     * people : [{"firstName":"ROY","lastName":"WILLIAMSON","statusIcon":"online","statusMessage":""},{"firstName":"BRAYDEN","lastName":"SCHROEDER","statusIcon":"offline","statusMessage":""},{"firstName":"WALTER","lastName":"HAMMOND","statusIcon":"busy","statusMessage":"busy"},{"firstName":"TIFFANY","lastName":"CLARKE","statusIcon":"online","statusMessage":""},{"firstName":"JACQUELINE","lastName":"NEWMAN","statusIcon":"away","statusMessage":""},{"firstName":"WILLIAM","lastName":"CLEMENTS","statusIcon":"online","statusMessage":"Happy"}]
     */

    private String groupName;
    private List<Person> people;

    public Group(String title, List<Person> items) {
        super(title, items);
        groupName = title;
        if (items != null) {
            people = new ArrayList<>(items);
        }
    }

    protected Group(Parcel in) {
        super(in);
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}
