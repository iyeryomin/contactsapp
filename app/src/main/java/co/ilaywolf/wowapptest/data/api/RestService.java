package co.ilaywolf.wowapptest.data.api;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RestService {
    @GET
    Single<ResponseBody> downloadJsonFile(@Url String url);
}
