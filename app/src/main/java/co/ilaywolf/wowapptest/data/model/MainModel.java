package co.ilaywolf.wowapptest.data.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import co.ilaywolf.wowapptest.App;
import co.ilaywolf.wowapptest.Constants;
import co.ilaywolf.wowapptest.data.entity.Group;
import co.ilaywolf.wowapptest.data.entity.Response;
import co.ilaywolf.wowapptest.di.Injection;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainModel implements Constants {

    public Single<List<Group>> getList() {
        Single<List<Group>> assetsSingle = Single.just(JSON_NAME)
                .map(file -> {
                    InputStream is = App.getContext().getAssets().open(file);
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    return parseJson(buffer);
                })
                .compose(applySchedulers());
        if (!isNetworkAvailable()) {
            return assetsSingle;
        } else {
            return Injection.getService().downloadJsonFile(JSON_URL)
                    .map(body -> parseJson(body.bytes()))
                    .onErrorResumeNext(assetsSingle)
                    .compose(applySchedulers());
        }
    }

    private List<Group> parseJson(byte[] bytes) throws IOException {
        String json = new String(bytes, StandardCharsets.UTF_8);
        Response response = new Gson().fromJson(json, Response.class);
        return response.getGroups();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityMgr = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private <T> SingleTransformer<T, T> applySchedulers() {
        return tObservable -> tObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
