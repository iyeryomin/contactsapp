package co.ilaywolf.wowapptest.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.ilaywolf.wowapptest.R;
import co.ilaywolf.wowapptest.data.entity.Group;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

class GroupVH extends GroupViewHolder {

    @BindView(R.id.item_group_text)
    TextView itemGroupText;

    @BindView(R.id.item_group_arrow)
    ImageView itemGroupArrow;

    private GroupVH(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    static GroupVH getInstance(ViewGroup parent) {
        return new GroupVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false));
    }

    void bind(Group group) {
        itemGroupText.setText(group.getGroupName());
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        itemGroupArrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        itemGroupArrow.setAnimation(rotate);
    }
}
