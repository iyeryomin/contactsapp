package co.ilaywolf.wowapptest.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.ilaywolf.wowapptest.Constants;
import co.ilaywolf.wowapptest.R;
import co.ilaywolf.wowapptest.data.entity.Person;

class PersonVH extends ChildViewHolder implements Constants {

    @BindView(R.id.item_person_name)
    TextView itemPersonName;
    @BindView(R.id.item_person_message)
    TextView itemPersonMessage;
    @BindView(R.id.item_person_avatar)
    ImageView itemPersonAvatar;
    @BindView(R.id.item_person_status)
    ImageView itemPersonStatus;

    private PersonVH(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    static PersonVH getInstance(ViewGroup parent) {
        return new PersonVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false));
    }

    void bind(Person person) {
        String fullName = itemView.getContext().getString(R.string.name_format, person.getFirstName(), person.getLastName());
        itemPersonName.setText(fullName);
        itemPersonMessage.setText(person.getStatusMessage());
        Glide.with(itemPersonAvatar).load(R.drawable.ic_avatar_male).into(itemPersonAvatar);
        Glide.with(itemPersonStatus).load(getResourceNameOfStatus(person)).into(itemPersonStatus);
    }

    @DrawableRes
    private int getResourceNameOfStatus(Person person) {
        switch (person.getStatusIcon()) {
            case ONLINE:
                return R.drawable.ic_online;
            case AWAY:
                return R.drawable.ic_away;
            case BUSY:
                return R.drawable.ic_busy;
            case OFFLINE:
                return R.drawable.ic_offline;
            case CALL_FORWARDING:
                return R.drawable.ic_call_forward;
            default:
                return R.drawable.ic_pending;
        }
    }
}
