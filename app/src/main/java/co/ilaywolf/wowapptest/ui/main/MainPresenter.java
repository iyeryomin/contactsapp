package co.ilaywolf.wowapptest.ui.main;

import java.util.ArrayList;
import java.util.List;

import co.ilaywolf.wowapptest.data.entity.Group;
import co.ilaywolf.wowapptest.data.entity.Person;
import co.ilaywolf.wowapptest.data.model.MainModel;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

class MainPresenter {

    private List<Group> groups = new ArrayList<>();

    private MainModel model = new MainModel();
    private MainView view;

    private CompositeDisposable disposable = new CompositeDisposable();

    void attachPresenter(MainView view) {

        this.view = view;
    }

    void detachPresenter() {
        disposable.dispose();
        view = null;
    }

    ArrayList<Group> getList() {
        Timber.d("Size "+groups.size());
        return new ArrayList<>(groups);
    }

    void setList(List<Group> list) {
        groups = new ArrayList<>(list);
    }

    void getContacts() {
        view.showProgress();
        disposable.add(model.getList()
                .subscribe(list -> {
                    groups = recreateList(list);
                    view.receivedList(groups);
                }, throwable -> {
                    Timber.e(throwable);
                    view.showError(throwable.getMessage());
                }));
    }

    /**
     * Recreate list for using Expandable list
     *
     * @param groups Initial list
     * @return New list with correct {@ExpandableGroup} items
     */
    private List<Group> recreateList(List<Group> groups) {
        List<Group> list = new ArrayList<>();
        for (Group group : groups) {
            if (!group.getPeople().isEmpty()) {
                list.add(new Group(group.getGroupName(), group.getPeople()));
            }
        }
        return list;
    }

    void search(String searchTerm) {
        if (!searchTerm.isEmpty()) {
            List<Group> tmpGroups = new ArrayList<>();
            for (Group group : groups) {
                List<Person> tmpPeople = new ArrayList<>();
                for (Person person : group.getPeople()) {
                    if (person.getFirstName().toLowerCase().contains(searchTerm)
                            || person.getLastName().toLowerCase().contains(searchTerm)
                            || person.getStatusMessage().toLowerCase().contains(searchTerm)
                            || person.getStatusIcon().toLowerCase().contains(searchTerm)) {
                        tmpPeople.add(person);
                    }
                }
                if (!tmpPeople.isEmpty()) {
                    tmpGroups.add(new Group(group.getGroupName(), tmpPeople));
                }
            }
            view.receivedList(tmpGroups);
        } else {
            view.receivedList(groups);
        }
    }
}
