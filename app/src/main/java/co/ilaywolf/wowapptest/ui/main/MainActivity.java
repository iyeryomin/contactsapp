package co.ilaywolf.wowapptest.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import co.ilaywolf.wowapptest.R;
import co.ilaywolf.wowapptest.data.entity.Group;
import co.ilaywolf.wowapptest.ui.main.adapter.MainAdapter;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.main_list)
    RecyclerView list;
    @BindView(R.id.main_search)
    EditText searchView;
    @BindView(R.id.main_progress)
    ProgressBar progress;

    private MainPresenter presenter = new MainPresenter();
    private MainAdapter adapter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        setupRecyclerView();

        presenter.attachPresenter(this);
        presenter.getContacts();

    }

    private void setupRecyclerView() {
        list.setLayoutManager(new LinearLayoutManager(this));
        list.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));

        RecyclerView.ItemAnimator animator = list.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        adapter = new MainAdapter(presenter.getList());
        list.setAdapter(adapter);
    }

    @OnTextChanged(value = R.id.main_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void search(CharSequence text) {
        presenter.search(text.toString().toLowerCase());
    }

    @OnEditorAction(R.id.main_search)
    public void onSearchPressed(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            searchView.clearFocus();
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (in != null) {
                in.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void receivedList(List<Group> list) {
        progress.setVisibility(View.GONE);
        adapter.submitList(list);

        //expand all groups
        for (int i = list.size() - 1; i > -1; i--) {
            adapter.expandGroup(i);
        }
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        Snackbar.make(list, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        presenter.detachPresenter();
        unbinder.unbind();
        super.onDestroy();
    }
}
