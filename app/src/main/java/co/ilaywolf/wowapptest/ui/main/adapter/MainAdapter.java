package co.ilaywolf.wowapptest.ui.main.adapter;

import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import co.ilaywolf.wowapptest.data.entity.Group;
import co.ilaywolf.wowapptest.data.entity.Person;

public class MainAdapter extends ExpandableRecyclerViewAdapter<GroupVH, PersonVH> {

    public MainAdapter(List<Group> groups) {
        super(groups);
    }

    @Override
    public GroupVH onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        return GroupVH.getInstance(parent);
    }

    @Override
    public PersonVH onCreateChildViewHolder(ViewGroup parent, int viewType) {
        return PersonVH.getInstance(parent);
    }

    @Override
    public void onBindChildViewHolder(PersonVH holder, int flatPosition, ExpandableGroup group, int childIndex) {
        holder.bind((Person) group.getItems().get(childIndex));
    }

    @Override
    public void onBindGroupViewHolder(GroupVH holder, int flatPosition, ExpandableGroup group) {
        holder.bind((Group) group);
    }

    public void submitList(List<Group> list) {
        List<Group> groups = (List<Group>) getGroups();
        groups.clear();
        groups.addAll(list);
        notifyGroupDataChanged();
        notifyDataSetChanged();
    }

    public void expandGroup(int position) {
        if (isGroupExpanded(position)) {
            return;
        }
        toggleGroup(position);
    }

    private void notifyGroupDataChanged() {
        expandableList.expandedGroupIndexes = new boolean[getGroups().size()];
        for (int i = 0; i < getGroups().size(); i++) {
            expandableList.expandedGroupIndexes[i] = false;
        }
    }
}
