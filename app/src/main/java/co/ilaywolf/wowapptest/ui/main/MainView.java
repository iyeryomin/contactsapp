package co.ilaywolf.wowapptest.ui.main;

import java.util.List;

import co.ilaywolf.wowapptest.data.entity.Group;

public interface MainView {
    void showProgress();

    void receivedList(List<Group> list);

    void showError(String error);
}
