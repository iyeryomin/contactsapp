package co.ilaywolf.wowapptest.di;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;

import co.ilaywolf.wowapptest.Constants;
import co.ilaywolf.wowapptest.data.api.RestService;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;

public class Injection implements Constants {

    private static File cacheDir;

    public static void init(Context context) {
        cacheDir = context.getCacheDir();
    }

    public static RestService getService() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getClient())
                .build()
                .create(RestService.class);
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .cache(getCache())
                .addInterceptor(chain ->
                {
                    Request request = chain.request();
                    request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                    return chain.proceed(request);
                })
                .build();
    }

    private static Cache getCache() {
        return new Cache(cacheDir, CACHE_SIZE);
    }
}
