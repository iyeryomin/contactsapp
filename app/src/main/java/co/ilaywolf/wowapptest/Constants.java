package co.ilaywolf.wowapptest;

public interface Constants {
    int CACHE_SIZE = 2 * 1024 * 1024;
    String JSON_URL = "https://file.wowapp.me/owncloud/index.php/s/F5WttwCODi1z3oo/download?path=%2F&files=contacts.json";
    String BASE_URL = "https://file.wowapp.me/";
    String JSON_NAME = "contacts.json";
    String ONLINE = "online";
    String AWAY = "away";
    String BUSY = "busy";
    String OFFLINE = "offline";
    String CALL_FORWARDING = "call forwarding";
}
