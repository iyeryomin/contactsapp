package co.ilaywolf.wowapptest;

import android.app.Application;
import android.content.Context;

import co.ilaywolf.wowapptest.di.Injection;
import timber.log.Timber;

public class App extends Application {

    //Should be replaced with normal dependency injection as Dagger2 or Koin
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());

        context = this;

        Injection.init(this);
    }
}
